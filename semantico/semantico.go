package semantico

import (
	"compilador/global"
	"compilador/lexico"
	"compilador/sintactico"
	"encoding/json"
	"fmt"
	"os"

	"github.com/fatih/color"
)

//Archivo
var archivoErrores *os.File

//Tabla de Simbolos
var tablaSimbolos global.TablaSimbolos

//Posicion Memoria
var posMem = 0

func error(mensaje string, fila int, col int) {
	color.Red("Error de Semántica en la Fila %d Col %d: %s \n", fila, col, mensaje)
	fmt.Fprintf(archivoErrores, "Error de Semántica en la Fila %d Col %d: %s \n", fila, col, mensaje)
}

//Analizar es la función principal del módulo semántico
func Analizar(path string) *global.Nodo {
	arbolSintactico := sintactico.Analizar(path)
	archivoErrores, _ = os.Create("errores-semanticos.fmc")
	tablaSimbolos = make(map[string]global.RowTabla)
	generarTablaSimbolos(arbolSintactico)
	//imprimeTabla(tablaSimbolos)
	arbolSintactico = revisionSemantica(arbolSintactico)
	//ImprimeArbol(arbolSintactico)
	return arbolSintactico
}

//Genera la Tabla de Simbolos a partir del Arbol Sintactico
func generarTablaSimbolos(arbolSintactico *global.Nodo) {
	if arbolSintactico != nil {
		insertarNodo(arbolSintactico)
		{
			for i := 0; i < 3; i++ {
				generarTablaSimbolos(arbolSintactico.Hijos[i])
			}
		}
		nullFunc(arbolSintactico)
		generarTablaSimbolos(arbolSintactico.Hermano)
	}
}

//Elimina las llamadas recursivas restantes
func nullFunc(arbolSintactico *global.Nodo) {
	if arbolSintactico != nil {
		return
	}
	return
}

//Inserta el nodo en la tabla de simbolos
func insertarNodo(arbolSintactico *global.Nodo) {
	switch arbolSintactico.TipoNodo {
	case sintactico.T_STMT:
		switch arbolSintactico.Tipo.Stmt {
		case sintactico.T_VAR:
			if _, ok := tablaSimbolos[arbolSintactico.Token.Lexema]; ok {
				error("La variable "+arbolSintactico.Token.Lexema+" ya se encuentra declarada.", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			} else {
				posMem++
				tablaSimbolos[arbolSintactico.Token.Lexema] = global.RowTabla{Nombre: arbolSintactico.Token.Lexema, Lineas: []int{arbolSintactico.Token.Fila}, PosMem: posMem, TipoDato: arbolSintactico.TipoDato}
			}
			break
		case sintactico.T_READ, sintactico.T_ASIGNA:
			/*fmt.Print("STMT: " + arbolSintactico.Token.Lexema + " Tipo: ")
			fmt.Println(arbolSintactico.Tipo.Stmt)*/
			//Si Existe Añade el Número de Linea, Si no, lo añade al mapa
			if simbolo, ok := tablaSimbolos[arbolSintactico.Hijos[0].Token.Lexema]; ok {
				simbolo.Lineas = append(simbolo.Lineas, arbolSintactico.Token.Fila)
				arbolSintactico.Hijos[0].TipoDato = simbolo.TipoDato
				tablaSimbolos[arbolSintactico.Hijos[0].Token.Lexema] = simbolo
			} else {
				error("La variable "+arbolSintactico.Hijos[0].Token.Lexema+" no está declarada.", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
				/*
					posMem++
					tablaSimbolos[arbolSintactico.Token.Lexema] = global.RowTabla{Nombre: arbolSintactico.Token.Lexema, Lineas: []int{arbolSintactico.Token.Fila}, PosMem: posMem}*/
			}
			break
		}
		break
	case sintactico.T_EXP:
		switch arbolSintactico.Tipo.Exp {
		case sintactico.T_ID:
			/*fmt.Print("EXP: " + arbolSintactico.Token.Lexema + " Tipo: ")
			fmt.Println(arbolSintactico.Tipo.Exp)*/
			//Si Existe Añade el Número de Linea, Si no, lo añade al mapa
			if simbolo, ok := tablaSimbolos[arbolSintactico.Token.Lexema]; ok {
				simbolo.Lineas = append(simbolo.Lineas, arbolSintactico.Token.Fila)
				arbolSintactico.TipoDato = simbolo.TipoDato
				tablaSimbolos[arbolSintactico.Token.Lexema] = simbolo
			} else {
				error("La variable "+arbolSintactico.Token.Lexema+" no está declarada.", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
				/*posMem++
				tablaSimbolos[arbolSintactico.Token.Lexema] = global.RowTabla{Nombre: arbolSintactico.Token.Lexema, Lineas: []int{arbolSintactico.Token.Fila}, PosMem: posMem}*/
			}
		}
		break
	}
}

func imprimeTabla(tablaSimbolos global.TablaSimbolos) {
	fmt.Printf("Nombre Variable \t\t PosMemoria \t\t Numero Lineas\n")
	fmt.Printf("--------------- \t\t ---------- \t\t -------------\n")
	for _, v := range tablaSimbolos {
		fmt.Printf("%-15s \t\t ", v.Nombre)
		fmt.Printf("%-10d \t\t ", v.PosMem)
		fmt.Println(v.Lineas)
	}
}

func imprimeTablaJSON(tablaSimbolos global.TablaSimbolos) {
	res, _ := json.Marshal(tablaSimbolos)
	fmt.Println(string(res))
}

//Revisón Semántica
func revisionSemantica(arbolSintactico *global.Nodo) *global.Nodo {
	if arbolSintactico != nil {
		nullFunc(arbolSintactico)
		{
			for i := 0; i < 3; i++ {
				revisionSemantica(arbolSintactico.Hijos[i])
			}
		}
		revisarNodo(arbolSintactico)
		revisionSemantica(arbolSintactico.Hermano)
	}
	return arbolSintactico
}

//Revisar Nodo
func revisarNodo(arbolSintactico *global.Nodo) {
	hijos := arbolSintactico.Hijos
	switch arbolSintactico.TipoNodo {
	case sintactico.T_EXP:
		switch arbolSintactico.Tipo.Exp {
		case sintactico.T_OP:
			if (hijos[0].TipoDato != sintactico.ENTERO && hijos[0].TipoDato != sintactico.FLOTANTE) || (hijos[1].TipoDato != sintactico.ENTERO && hijos[1].TipoDato != sintactico.FLOTANTE) {
				error("Operación aplicada a variables no númericas", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			} else if arbolSintactico.Token.Tipo == sintactico.TKN_MENOR || arbolSintactico.Token.Tipo == sintactico.TKN_MENORIGUAL || arbolSintactico.Token.Tipo == sintactico.TKN_MAYOR || arbolSintactico.Token.Tipo == sintactico.TKN_MAYORIGUAL {
				arbolSintactico.TipoDato = sintactico.BOOLEAN
			} else if hijos[0].TipoDato == sintactico.FLOTANTE || hijos[1].TipoDato == sintactico.FLOTANTE {
				arbolSintactico.TipoDato = sintactico.FLOTANTE
			} else {
				arbolSintactico.TipoDato = sintactico.ENTERO
			}
			break
		case sintactico.T_OP_BOOL:
			if arbolSintactico.Token.Tipo == sintactico.TKN_NOT && hijos[0].TipoDato != sintactico.BOOLEAN {
				error(lexico.TraduceTipoToken(arbolSintactico.Token.Tipo)+" aplicado a variable no booleana", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			} else if hijos[0].TipoDato != sintactico.BOOLEAN || hijos[1].TipoDato != sintactico.BOOLEAN {
				error(lexico.TraduceTipoToken(arbolSintactico.Token.Tipo)+" aplicado a variables no booleanas", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			} else {
				arbolSintactico.TipoDato = sintactico.BOOLEAN
			}
			break
		case sintactico.T_OP_AMBOS:
			arbolSintactico.TipoDato = sintactico.BOOLEAN
			break
		case sintactico.T_CONST, sintactico.T_ID:
			//Se asigna en el sintáctico
			break
		}
	case sintactico.T_STMT:
		switch arbolSintactico.Tipo.Stmt {
		case sintactico.T_WRITE:
			if hijos[0].TipoDato != sintactico.ENTERO && hijos[0].TipoDato != sintactico.FLOTANTE {
				error(lexico.TraduceTipoToken(arbolSintactico.Token.Tipo)+" aplicado a variables no númericas", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			}
			break
		case sintactico.T_IF, sintactico.T_WHILE:
			if hijos[0].TipoDato != sintactico.BOOLEAN {
				error(lexico.TraduceTipoToken(arbolSintactico.Token.Tipo)+" aplicado a variables no booleanas", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			}
			break
		case sintactico.T_REPEAT:
			if hijos[1].Hijos[0].TipoDato != sintactico.BOOLEAN {
				error(lexico.TraduceTipoToken(arbolSintactico.Token.Tipo)+" aplicado a variables no booleanas", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			}
			break
		case sintactico.T_ASIGNA:
			if (hijos[0].TipoDato == sintactico.BOOLEAN && hijos[1].TipoDato != sintactico.BOOLEAN) || (hijos[0].TipoDato != sintactico.BOOLEAN && hijos[1].TipoDato == sintactico.BOOLEAN) {
				error("La asignación no corresponde al tipo de la variable "+hijos[0].Token.Lexema+" ( "+sintactico.TraduceTipoDato(hijos[0].TipoDato)+" )", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
			} else if hijos[0].TipoDato == sintactico.ENTERO && hijos[1].TipoDato == sintactico.FLOTANTE {
				error("Pérdida de precisión al asignar "+hijos[0].Token.Lexema+" ( "+sintactico.TraduceTipoDato(hijos[0].TipoDato)+" )", arbolSintactico.Token.Fila, arbolSintactico.Token.Col)
				arbolSintactico.TipoDato = sintactico.ENTERO
			} else if hijos[0].TipoDato == sintactico.FLOTANTE && hijos[1].TipoDato == sintactico.FLOTANTE {
				arbolSintactico.TipoDato = sintactico.FLOTANTE
			} else if hijos[0].TipoDato == sintactico.ENTERO || hijos[1].TipoDato == sintactico.ENTERO {
				arbolSintactico.TipoDato = sintactico.ENTERO
			} else {
				arbolSintactico.TipoDato = sintactico.BOOLEAN
			}
			break
		}
		break
	}
}

var tab = -1

//ImprimeArbol muestra el arbol sintáctico (con tipos de datos asignados) en consola
func ImprimeArbol(t *global.Nodo) {
	var i int
	var cadena, tabu string
	tabu = "	"
	tab++
	for t != nil {
		i = 0
		for i < tab {
			fmt.Print(tabu)
			i++
		}
		cadena = t.Token.Lexema
		fmt.Println(cadena + " ( " + sintactico.TraduceTipoDato(t.TipoDato) + " )")
		for i := 0; i < 3; i++ {
			ImprimeArbol(t.Hijos[i])
		}
		t = t.Hermano
	}
	tab--
}
