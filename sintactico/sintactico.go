package sintactico

import (
	"compilador/global"
	"compilador/lexico"
	"encoding/json"
	"fmt"
	"os"
	"strconv"

	"github.com/fatih/color"
)

//Tipos Tokens
const (
	TKN_PROGRAM global.Tipos = iota
	TKN_IF
	TKN_ELSE
	TKN_FI
	TKN_DO
	TKN_UNTIL
	TKN_WHILE
	TKN_READ
	TKN_WRITE
	TKN_FLOAT
	TKN_INT
	TKN_BOOL
	TKN_NOT
	TKN_AND
	TKN_OR
	TKN_TRUE
	TKN_FALSE
	TKN_MAS
	TKN_MENOS
	TKN_POR
	TKN_ENTRE
	TKN_POTENCIA
	TKN_MENOR
	TKN_MENORIGUAL
	TKN_MAYOR
	TKN_MAYORIGUAL
	TKN_IGUAL
	TKN_DIFERENTE
	TKN_ASIGNA
	TKN_PUNTOYCOMA
	TKN_COMA
	TKN_PARENI
	TKN_PAREND
	TKN_LLAVEI
	TKN_LLAVED
	TKN_ID
	TKN_NUMERO
	TKN_DECIMAL
	TKN_COMENTARIO_SIMPLE
	TKN_COMENTARIO_MULTIPLE_ABRE
	TKN_COMENTARIO_MULTIPLE_CONTENIDO
	TKN_COMENTARIO_MULTIPLE_CIERRA
	TKN_COMENTARIO_MULTIPLE
	TKN_EOF
	TKN_ERROR
)

//Tipos Nodo
const (
	T_NODO_NULL global.TipoNodo = iota
	T_STMT
	T_EXP
)

//Tipos Stmt
const (
	T_STMT_NULL global.TipoStmt = iota
	T_ASIGNA
	T_READ
	T_WRITE
	T_BLOQUE
	T_REPEAT
	T_WHILE
	T_IF
	T_ELSE
	T_VAR
)

//Tipos Exp
const (
	T_EXP_NULL global.TipoExp = iota
	T_OP
	T_OP_BOOL
	T_OP_AMBOS
	T_CONST
	T_ID
)

//Tipo Dato
const (
	VOID global.TipoDato = iota
	ENTERO
	FLOTANTE
	BOOLEAN
)

//Token Actual
var tokenActual global.Token

//Conjunto de tokens
var tokens []global.Token

//Archivo
var archivoErrores *os.File

func obtenerTokens(path string) {
	tokens = lexico.Analizar(path)
}

func getToken() global.Token {
	token, arr := tokens[0], tokens[1:]
	tokens = arr
	return token
}

func error(mensaje string) {
	color.Red("Error de Sintaxis en la Fila %d Col %d: %s \n", tokenActual.Fila, tokenActual.Col, mensaje)
	fmt.Fprintf(archivoErrores, "Error de Sintaxis en la Fila %d Col %d: %s \n", tokenActual.Fila, tokenActual.Col, mensaje)
}

func tokenEsperado(token global.Tipos) {
	if token == tokenActual.Tipo {
		tokenActual = getToken()
	} else {
		error("Token Inesperado -> " + lexico.TraduceTipoToken(tokenActual.Tipo) + " Se esperaba -> " + lexico.TraduceTipoToken(token))
	}
}

func nuevoNodoStmt(tipo global.TipoStmt) *global.Nodo {
	t := &global.Nodo{}
	t.Hermano = nil
	t.Tipo.Stmt = tipo
	t.TipoNodo = T_STMT
	return t
}

func nuevoNodoExp(tipo global.TipoExp) *global.Nodo {
	t := &global.Nodo{}
	t.Hermano = nil
	t.Tipo.Exp = tipo
	t.TipoNodo = T_EXP
	t.TipoDato = VOID
	return t
}

//Analizar es la función principal del módulo sintáctico, regresa un Nodo con el arbol sintáctico
func Analizar(path string) *global.Nodo {
	t := &global.Nodo{}
	obtenerTokens(path)
	tokenActual = getToken()
	if tokenActual.Tipo == TKN_PROGRAM {
		t.Token = tokenActual
	}
	tokenEsperado(TKN_PROGRAM)
	tokenEsperado(TKN_LLAVEI)
	t.Hijos[0] = listaDeclaracion()
	t.Hijos[1] = listaSentencias()
	tokenEsperado(TKN_LLAVED)
	//ImprimeArbol(t)
	return t
}

var tab = -1

//ImprimeArbol muestra el arbol sintáctico en consola
func ImprimeArbol(t *global.Nodo) {
	var i int
	var cadena, tabu string
	tabu = "	"
	tab++
	for t != nil {
		i = 0
		for i < tab {
			fmt.Print(tabu)
			i++
		}
		cadena = t.Token.Lexema
		fmt.Print(cadena)
		fmt.Print("\r\n")
		for i := 0; i < 3; i++ {
			ImprimeArbol(t.Hijos[i])
		}
		t = t.Hermano
	}
	tab--
}

var nivel = -1

func generaArbol(t *global.Nodo, f *os.File) {
	nivel++
	for t != nil {
		hoja := &global.Hoja{Token: t.Token, Nivel: nivel}
		res, _ := json.Marshal(hoja)
		fmt.Fprintln(f, string(res))
		for i := 0; i < 3; i++ {
			generaArbol(t.Hijos[i], f)
		}
		t = t.Hermano
	}
	nivel--
}

func listaDeclaracion() *global.Nodo {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_INT, TKN_FLOAT, TKN_BOOL, TKN_EOF:
		t = declaracion()
		if t != nil {
			t.Hermano = listaDeclaracion()
		}
		break
	default:
		return nil
	}
	return t
}

func declaracion() *global.Nodo {
	t := &global.Nodo{}
	t = tipo()
	t.Hijos[0] = listaID(t.TipoDato)
	tokenEsperado(TKN_PUNTOYCOMA)
	return t
}

func tipo() *global.Nodo {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_INT:
		t.Token = tokenActual
		t.TipoDato = ENTERO
		tokenEsperado(tokenActual.Tipo)
		break
	case TKN_FLOAT:
		t.Token = tokenActual
		t.TipoDato = FLOTANTE
		tokenEsperado(tokenActual.Tipo)
		break
	case TKN_BOOL:
		t.Token = tokenActual
		t.TipoDato = BOOLEAN
		tokenEsperado(tokenActual.Tipo)
		break
	default:
		t = nil
	}
	return t
}

func listaID(tipoDato global.TipoDato) *global.Nodo {
	t := &global.Nodo{}
	if tokenActual.Tipo == TKN_ID {
		t = nuevoNodoStmt(T_VAR)
		t.Token = tokenActual
		t.TipoDato = tipoDato
		tokenEsperado(TKN_ID)
		t.Hermano = listaID(tipoDato)
	} else if tokenActual.Tipo == TKN_COMA {
		tokenEsperado(TKN_COMA)
		t = listaID(tipoDato)
	} else {
		t = nil
	}
	return t
}

func listaSentencias() *global.Nodo {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_IF, TKN_WHILE, TKN_DO, TKN_READ, TKN_WRITE, TKN_LLAVEI, TKN_ID:
		t = sentencia()
		if t != nil {
			t.Hermano = listaSentencias()
		}
		break
	default:
		return nil
	}
	return t
}

func sentencia() *global.Nodo {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_IF:
		t = seleccion()
		break
	case TKN_WHILE:
		t = iteracion()
		break
	case TKN_DO:
		t = repeticion()
		break
	case TKN_READ:
		t = stmtRead()
		break
	case TKN_WRITE:
		t = stmtWrite()
		break
	case TKN_LLAVEI:
		t = bloque()
		break
	case TKN_ID:
		t = asignacion()
		break
	default:
		t = nil
		error("Token Inesperado -> " + lexico.TraduceTipoToken(tokenActual.Tipo))
		error("Se esperaba una sentencia")
		tokenActual = getToken()
		break
	}
	return t
}

func seleccion() *global.Nodo {
	t, exp, tElse := &global.Nodo{}, &global.Nodo{}, &global.Nodo{}
	t = nuevoNodoStmt(T_IF)
	t.Token = tokenActual
	tokenEsperado(TKN_IF)
	tokenEsperado(TKN_PARENI)
	expresion(exp)
	t.Hijos[0] = exp.Hijos[0]
	tokenEsperado(TKN_PAREND)
	t.Hijos[1] = bloque()
	if tokenActual.Tipo == TKN_ELSE {
		tElse.Token = tokenActual
		tokenEsperado(TKN_ELSE)
		tElse.Hijos[0] = bloque()
		t.Hijos[2] = tElse
	}
	tokenEsperado(TKN_FI)
	return t
}

func iteracion() *global.Nodo {
	t, exp, cont := &global.Nodo{}, &global.Nodo{}, &global.Nodo{}
	t = nuevoNodoStmt(T_WHILE)
	t.Token = tokenActual
	tokenEsperado(TKN_WHILE)
	tokenEsperado(TKN_PARENI)
	expresion(exp)
	t.Hijos[0] = exp.Hijos[0]
	tokenEsperado(TKN_PAREND)
	cont = bloque()
	t.Hijos[1] = cont
	return t
}

func repeticion() *global.Nodo {
	t, exp, exp2, cont := &global.Nodo{}, &global.Nodo{}, &global.Nodo{}, &global.Nodo{}
	t = nuevoNodoStmt(T_REPEAT)
	t.Token = tokenActual
	tokenEsperado(TKN_DO)
	cont = bloque()
	t.Hijos[0] = cont
	if tokenActual.Tipo == TKN_UNTIL {
		exp.Token = tokenActual
		exp.TipoNodo = T_STMT
	}
	tokenEsperado(TKN_UNTIL)
	tokenEsperado(TKN_PARENI)
	expresion(exp2)
	exp.Hijos[0] = exp2.Hijos[0]
	tokenEsperado(TKN_PAREND)
	t.Hijos[1] = exp
	tokenEsperado(TKN_PUNTOYCOMA)
	return t
}

func stmtRead() *global.Nodo {
	t, id := &global.Nodo{}, &global.Nodo{}
	t = nuevoNodoStmt(T_READ)
	t.Token = tokenActual
	tokenEsperado(TKN_READ)
	if tokenActual.Tipo == TKN_ID {
		id.Token = tokenActual
	}
	tokenEsperado(TKN_ID)
	t.Hijos[0] = id
	tokenEsperado(TKN_PUNTOYCOMA)
	return t
}

func stmtWrite() *global.Nodo {
	t := &global.Nodo{}
	t = nuevoNodoStmt(T_WRITE)
	t.Token = tokenActual
	tokenEsperado(TKN_WRITE)
	expresion(t)
	tokenEsperado(TKN_PUNTOYCOMA)
	return t
}

func bloque() *global.Nodo {
	t := &global.Nodo{}
	t = nuevoNodoStmt(T_BLOQUE)
	tokenEsperado(TKN_LLAVEI)
	t = listaSentencias()
	tokenEsperado(TKN_LLAVED)
	return t
}

func asignacion() *global.Nodo {
	t, id, exp := &global.Nodo{}, &global.Nodo{}, &global.Nodo{}
	t = nuevoNodoStmt(T_ASIGNA)
	exp = nuevoNodoExp(T_OP)
	if tokenActual.Tipo == TKN_ID {
		t.Token = tokenActual
		id.Token = tokenActual
	}
	t.Hijos[0] = id
	tokenEsperado(TKN_ID)
	tokenEsperado(TKN_ASIGNA)
	expresion(exp)
	t.Hijos[1] = exp.Hijos[0]
	tokenEsperado(TKN_PUNTOYCOMA)
	return t
}

func expresion(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_OR:
		t = nuevoNodoExp(T_OP_BOOL)
		t.Token = tokenActual
		tokenEsperado(TKN_OR)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		termAnd(t)
		break
	default:
		termAnd(todos)
	}
	if tokenActual.Tipo == TKN_OR {
		expresion(todos)
	}
}

func termAnd(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_AND:
		t = nuevoNodoExp(T_OP_BOOL)
		t.Token = tokenActual
		tokenEsperado(TKN_AND)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		igualdad(t)
		break
	default:
		igualdad(todos)
	}
	if tokenActual.Tipo == TKN_AND {
		termAnd(todos)
	}
}

func igualdad(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_IGUAL, TKN_DIFERENTE:
		t = nuevoNodoExp(T_OP_AMBOS)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		relacion(t)
		break
	default:
		relacion(todos)
	}
	if tokenActual.Tipo == TKN_IGUAL || tokenActual.Tipo == TKN_DIFERENTE {
		igualdad(todos)
	}
}

func relacion(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_MAYOR, TKN_MENOR, TKN_MAYORIGUAL, TKN_MENORIGUAL:
		t = nuevoNodoExp(T_OP)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		expSuma(t)
		break
	default:
		expSuma(todos)
	}
	if tokenActual.Tipo == TKN_MAYOR || tokenActual.Tipo == TKN_MENOR || tokenActual.Tipo == TKN_MAYORIGUAL || tokenActual.Tipo == TKN_MENORIGUAL {
		relacion(todos)
	}
}

func expSuma(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_MAS, TKN_MENOS:
		t = nuevoNodoExp(T_OP)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		termino(t)
		break
	default:
		termino(todos)
	}
	if tokenActual.Tipo == TKN_MAS || tokenActual.Tipo == TKN_MENOS {
		expSuma(todos)
	}
}

func termino(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_POR, TKN_ENTRE:
		t = nuevoNodoExp(T_OP)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[1] == nil {
			t.Hijos[0] = todos.Hijos[0]
			todos.Hijos[0] = t
		} else {
			t.Hijos[0] = todos.Hijos[1]
			todos.Hijos[1] = t
		}
		unario(t)
		break
	default:
		unario(todos)
	}
	if tokenActual.Tipo == TKN_POR || tokenActual.Tipo == TKN_ENTRE {
		termino(todos)
	}
}

func unario(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_MAS, TKN_MENOS:
		t = nuevoNodoExp(T_OP)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		factor(todos)
		if todos.Hijos[0] == nil {
			t.Hijos[0] = t
		} else {
			todos.Hijos[1] = t
		}
		break
	case TKN_NOT:
		t = nuevoNodoExp(T_OP_BOOL)
		t.Token = tokenActual
		tokenEsperado(tokenActual.Tipo)
		factor(todos)
		if todos.Hijos[0] == nil {
			t.Hijos[0] = t
		} else {
			todos.Hijos[1] = t
		}
		break
	default:
		factor(todos)
	}
}

func factor(todos *global.Nodo) {
	t := &global.Nodo{}
	switch tokenActual.Tipo {
	case TKN_NUMERO:
		t = nuevoNodoExp(T_CONST)
		t.TipoDato = ENTERO
		t.Attr.ValI, _ = strconv.Atoi(tokenActual.Lexema)
		t.Token = tokenActual
		tokenEsperado(TKN_NUMERO)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		}
		break
	case TKN_DECIMAL:
		t = nuevoNodoExp(T_CONST)
		t.TipoDato = FLOTANTE
		t.Attr.ValF, _ = strconv.ParseFloat(tokenActual.Lexema, 64)
		t.Token = tokenActual
		tokenEsperado(TKN_DECIMAL)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		}
		break
	case TKN_ID:
		t = nuevoNodoExp(T_ID)
		t.Token = tokenActual
		tokenEsperado(TKN_ID)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		}
		break
	case TKN_TRUE:
		t = nuevoNodoExp(T_CONST)
		t.TipoDato = BOOLEAN
		t.Token = tokenActual
		t.Attr.ValB = true
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		}
		break
	case TKN_FALSE:
		t = nuevoNodoExp(T_CONST)
		t.TipoDato = BOOLEAN
		t.Token = tokenActual
		t.Attr.ValB = false
		tokenEsperado(tokenActual.Tipo)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		}
		break
	case TKN_PARENI:
		tokenEsperado(TKN_PARENI)
		expresion(t)
		tokenEsperado(TKN_PAREND)
		if todos.Hijos[0] == nil {
			todos.Hijos[0] = t.Hijos[0]
		} else if todos.Hijos[1] == nil {
			todos.Hijos[1] = t
		} else {
			t.Hijos[0] = todos
			todos = t
		}
		break
	default:
		todos = nil
		error("Token Inesperado -> " + lexico.TraduceTipoToken(tokenActual.Tipo))
		error("Se esperaba factor")
		tokenActual = getToken()
	}
}

//TraduceTipoDato muestra el valor en string del tipo de dato
func TraduceTipoDato(Tipo global.TipoDato) string {
	tokens := map[global.TipoDato]string{
		ENTERO:   "ENTERO",
		FLOTANTE: "FLOTANTE",
		BOOLEAN:  "BOOLEAN",
	}
	if palabra, ok := tokens[Tipo]; ok {
		return palabra
	}
	return ""
}
