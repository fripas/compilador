package global

type Tipos int

type Token struct {
	Tipo   Tipos
	Lexema string
	Fila   int
	Col    int
}

type TipoNodo int
type TipoStmt int
type TipoExp int
type TipoDato int

type Nodo struct {
	Hijos    [3]*Nodo
	Hermano  *Nodo
	TipoNodo TipoNodo
	Tipo     struct {
		Stmt TipoStmt
		Exp  TipoExp
	}
	Attr struct {
		ValI int
		ValF float64
		ValB bool
	}
	TipoDato TipoDato
	Token    Token
}

type Hoja struct {
	Nivel int
	Token Token
}

type RowTabla struct {
	Nombre   string
	Lineas   []int
	PosMem   int
	TipoDato TipoDato
}

type TablaSimbolos map[string]RowTabla
