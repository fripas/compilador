package lexico

import (
	"bufio"
	"compilador/global"
	"os"
	"strings"
	"unicode"
)

//Estados es un tipo definido para identificar el estado del automata seleccionado
type Estados int

const (
	EN_INICIO Estados = iota
	EN_MENOR
	EN_MAYOR
	EN_IGUAL
	EN_DIAGONAL
	EN_ASTERISCO
	EN_DIFERENTE
	EN_ID
	EN_NUMERO
	EN_PUNTO
	EN_COMENTARIO_SIMPLE
	EN_ERROR
	EN_LISTO
)

const (
	TKN_PROGRAM global.Tipos = iota
	TKN_IF
	TKN_ELSE
	TKN_FI
	TKN_DO
	TKN_UNTIL
	TKN_WHILE
	TKN_READ
	TKN_WRITE
	TKN_FLOAT
	TKN_INT
	TKN_BOOL
	TKN_NOT
	TKN_AND
	TKN_OR
	TKN_TRUE
	TKN_FALSE
	TKN_MAS
	TKN_MENOS
	TKN_POR
	TKN_ENTRE
	TKN_POTENCIA
	TKN_MENOR
	TKN_MENORIGUAL
	TKN_MAYOR
	TKN_MAYORIGUAL
	TKN_IGUAL
	TKN_DIFERENTE
	TKN_ASIGNA
	TKN_PUNTOYCOMA
	TKN_COMA
	TKN_PARENI
	TKN_PAREND
	TKN_LLAVEI
	TKN_LLAVED
	TKN_ID
	TKN_NUMERO
	TKN_DECIMAL
	TKN_COMENTARIO_SIMPLE
	TKN_COMENTARIO_MULTIPLE_ABRE
	TKN_COMENTARIO_MULTIPLE_CONTENIDO
	TKN_COMENTARIO_MULTIPLE_CIERRA
	TKN_COMENTARIO_MULTIPLE
	TKN_EOF
	TKN_ERROR
)

//Analizar es la función principal del módulo léxico, regresa un array de Token
func Analizar(path string) []global.Token {
	archivo, _ := os.Open(path)
	defer archivo.Close()
	scanner := bufio.NewScanner(archivo)
	scanner.Split(bufio.ScanLines)
	var Fila, Col int = 0, 0
	var comentarioMultiple bool
	var linea string
	var token, tokenComentarioMultiple global.Token
	var tokens []global.Token
	for scanner.Scan() {
		Col = 0
		linea = strings.TrimSpace(scanner.Text())
		for Col < len(linea) {
			token, Col = getToken(linea, Fila, Col)
			if token.Tipo == TKN_COMENTARIO_MULTIPLE_ABRE {
				comentarioMultiple = true
				tokenComentarioMultiple = token
			} else if comentarioMultiple && token.Tipo != TKN_COMENTARIO_MULTIPLE_CIERRA {
				tokenComentarioMultiple.Lexema += token.Lexema
			} else if comentarioMultiple {
				comentarioMultiple = false
				tokenComentarioMultiple.Lexema += token.Lexema
				token = tokenComentarioMultiple
				token.Tipo = TKN_COMENTARIO_MULTIPLE
				//tokens = append(tokens, token)
			} else if token.Tipo != TKN_COMENTARIO_SIMPLE && token.Tipo != TKN_ERROR {
				tokens = append(tokens, token)
			}
		}
		Fila++
	}
	tokens = append(tokens, global.Token{Tipo: TKN_EOF})
	return tokens
}

func getToken(linea string, Fila int, Col int) (global.Token, int) {
	var (
		estado = EN_INICIO
		c      rune
		token  global.Token
	)
	tokenSimple := map[rune]global.Tipos{
		'+': TKN_MAS,
		'-': TKN_MENOS,
		'^': TKN_POTENCIA,
		';': TKN_PUNTOYCOMA,
		',': TKN_COMA,
		'(': TKN_PARENI,
		')': TKN_PAREND,
		'{': TKN_LLAVEI,
		'}': TKN_LLAVED,
	}
	for estado != EN_LISTO {
		switch estado {
		case EN_INICIO:
			c = rune(linea[Col])
			for esDelim(c) {
				Col++
				if Col < len(linea) {
					c = rune(linea[Col])
				} else {
					return token, 0
				}
			}
			if unicode.IsLetter(c) {
				estado = EN_ID
				token.Lexema += string(c)
				Col++
			} else if unicode.IsDigit(c) {
				estado = EN_NUMERO
				token.Lexema += string(c)
				Col++
			} else if Tipo, ok := tokenSimple[c]; ok {
				estado = EN_LISTO
				token.Tipo = Tipo
				token.Lexema += string(c)
				Col++
			} else if c == '/' {
				estado = EN_DIAGONAL
				token.Lexema += string(c)
				Col++
			} else if c == '*' {
				estado = EN_ASTERISCO
				token.Lexema += string(c)
				Col++
			} else if c == '<' {
				estado = EN_MENOR
				token.Lexema += string(c)
				Col++
			} else if c == '>' {
				estado = EN_MAYOR
				token.Lexema += string(c)
				Col++
			} else if c == '=' {
				estado = EN_IGUAL
				token.Lexema += string(c)
				Col++
			} else if c == '!' {
				estado = EN_DIFERENTE
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_ERROR
				token.Lexema += string(c)
				Col++
			}
			break
		case EN_NUMERO:
			if Col >= len(linea) {
				estado = EN_LISTO
				token.Tipo = TKN_NUMERO
				break
			}
			c = rune(linea[Col])
			if c == '.' {
				token.Lexema += string(c)
				Col++
				estado = EN_PUNTO
			} else if !unicode.IsDigit(c) {
				estado = EN_LISTO
				token.Tipo = TKN_NUMERO
			} else {
				token.Lexema += string(c)
				Col++
			}
			break
		case EN_PUNTO:
			if Col >= len(linea) {
				estado = EN_LISTO
				token.Tipo = TKN_DECIMAL
				break
			}
			c = rune(linea[Col])
			if !unicode.IsDigit(c) {
				estado = EN_LISTO
				token.Tipo = TKN_DECIMAL
			} else {
				token.Lexema += string(c)
				Col++
			}
			break
		case EN_ID:
			if Col >= len(linea) {
				estado = EN_LISTO
				token = buscarPalabrasReservadas(token.Lexema)
				break
			}
			c = rune(linea[Col])
			if !unicode.IsLetter(c) && !unicode.IsDigit(c) {
				estado = EN_LISTO
				token = buscarPalabrasReservadas(token.Lexema)
			} else {
				token.Lexema += string(c)
				Col++
			}
			break
		case EN_DIAGONAL:
			c = rune(linea[Col])
			if c == '/' {
				estado = EN_COMENTARIO_SIMPLE
				token.Lexema += string(c)
				Col++
			} else if c == '*' {
				estado = EN_LISTO
				token.Tipo = TKN_COMENTARIO_MULTIPLE_ABRE
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_ENTRE
			}
			break
		case EN_ASTERISCO:
			c = rune(linea[Col])
			if c == '/' {
				estado = EN_LISTO
				token.Tipo = TKN_COMENTARIO_MULTIPLE_CIERRA
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_POR
			}
			break
		case EN_COMENTARIO_SIMPLE:
			if Col >= len(linea) {
				estado = EN_LISTO
				token.Tipo = TKN_COMENTARIO_SIMPLE
				break
			}
			c = rune(linea[Col])
			token.Lexema += string(c)
			Col++
			break
		case EN_MENOR:
			c = rune(linea[Col])
			if c == '=' {
				estado = EN_LISTO
				token.Tipo = TKN_MENORIGUAL
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_MENOR
			}
			break
		case EN_MAYOR:
			c = rune(linea[Col])
			if c == '=' {
				estado = EN_LISTO
				token.Tipo = TKN_MAYORIGUAL
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_MAYOR
			}
			break
		case EN_IGUAL:
			c = rune(linea[Col])
			if c == '=' {
				estado = EN_LISTO
				token.Tipo = TKN_IGUAL
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_ASIGNA
			}
			break
		case EN_DIFERENTE:
			c = rune(linea[Col])
			if c == '=' {
				estado = EN_LISTO
				token.Tipo = TKN_DIFERENTE
				token.Lexema += string(c)
				Col++
			} else {
				estado = EN_LISTO
				token.Tipo = TKN_ERROR
			}
			break
		default:
			token.Tipo = TKN_ERROR
			estado = EN_LISTO
			token.Lexema += string(c)
			Col++
		}
	}
	token.Fila = Fila + 1
	token.Col = (Col - len(token.Lexema)) + 1
	return token, Col
}

func esDelim(c rune) bool {
	return unicode.IsSpace(c)
}

func buscarPalabrasReservadas(palabra string) global.Token {
	palabrasReservadas := map[string]global.Tipos{
		"program": TKN_PROGRAM,
		"if":      TKN_IF,
		"else":    TKN_ELSE,
		"fi":      TKN_FI,
		"do":      TKN_DO,
		"until":   TKN_UNTIL,
		"while":   TKN_WHILE,
		"read":    TKN_READ,
		"write":   TKN_WRITE,
		"float":   TKN_FLOAT,
		"int":     TKN_INT,
		"bool":    TKN_BOOL,
		"not":     TKN_NOT,
		"and":     TKN_AND,
		"or":      TKN_OR,
		"true":    TKN_TRUE,
		"false":   TKN_FALSE,
		"TRUE":    TKN_TRUE,
		"FALSE":   TKN_FALSE,
	}
	if Tipo, ok := palabrasReservadas[palabra]; ok {
		return global.Token{Tipo: Tipo, Lexema: palabra, Fila: 0, Col: 0}
	}
	return global.Token{Tipo: TKN_ID, Lexema: palabra, Fila: 0, Col: 0}
}

//TraduceTipoToken imprime como string el tipo del token
func TraduceTipoToken(Tipo global.Tipos) string {
	tokens := map[global.Tipos]string{
		TKN_PROGRAM:             "TKN_PROGRAM",
		TKN_IF:                  "TKN_IF",
		TKN_ELSE:                "TKN_ELSE",
		TKN_FI:                  "TKN_FI",
		TKN_DO:                  "TKN_DO",
		TKN_UNTIL:               "TKN_UNTIL",
		TKN_WHILE:               "TKN_WHILE",
		TKN_READ:                "TKN_READ",
		TKN_WRITE:               "TKN_WRITE",
		TKN_FLOAT:               "TKN_FLOAT",
		TKN_INT:                 "TKN_INT",
		TKN_BOOL:                "TKN_BOOL",
		TKN_NOT:                 "TKN_NOT",
		TKN_AND:                 "TKN_AND",
		TKN_OR:                  "TKN_OR",
		TKN_TRUE:                "TKN_TRUE",
		TKN_FALSE:               "TKN_FALSE",
		TKN_MAS:                 "TKN_MAS",
		TKN_MENOS:               "TKN_MENOS",
		TKN_POR:                 "TKN_POR",
		TKN_ENTRE:               "TKN_ENTRE",
		TKN_POTENCIA:            "TKN_POTENCIA",
		TKN_MENOR:               "TKN_MENOR",
		TKN_MENORIGUAL:          "TKN_MENORIGUAL",
		TKN_MAYOR:               "TKN_MAYOR",
		TKN_MAYORIGUAL:          "TKN_MAYORIGUAL",
		TKN_IGUAL:               "TKN_IGUAL",
		TKN_DIFERENTE:           "TKN_DIFERENTE",
		TKN_ASIGNA:              "TKN_ASIGNA",
		TKN_PUNTOYCOMA:          "TKN_PUNTOYCOMA",
		TKN_COMA:                "TKN_COMA",
		TKN_PARENI:              "TKN_PARENI",
		TKN_PAREND:              "TKN_PAREND",
		TKN_LLAVEI:              "TKN_LLAVEI",
		TKN_LLAVED:              "TKN_LLAVED",
		TKN_ID:                  "TKN_ID",
		TKN_NUMERO:              "TKN_NUMERO",
		TKN_DECIMAL:             "TKN_DECIMAL",
		TKN_EOF:                 "TKN_EOF",
		TKN_ERROR:               "TKN_ERROR",
		TKN_COMENTARIO_SIMPLE:   "TKN_COMENTARIO_SIMPLE",
		TKN_COMENTARIO_MULTIPLE: "TKN_COMENTARIO_MULTIPLE",
	}
	if palabra, ok := tokens[Tipo]; ok {
		return palabra
	}
	return ""
}
