package main

import (
	"compilador/global"
	"compilador/semantico"
	"compilador/sintactico"
	"fmt"
	"os"
	"strconv"
)

//Archivo de Codigo
var txtCodigo *os.File
var hl int

func main() {
	if len(os.Args) == 2 {
		archivo := os.Args[1]
		//fmt.Printf("El archivo leído es: %s \n", archivo)
		txtCodigo, _ = os.Create("codigo-intermedio.fmc")
		cGen(semantico.Analizar(archivo))
	} else {
		fmt.Println("Se necesita un archivo como parámetro")
	}
}

//Comentario
func emitComment(c string) {
	fmt.Print("\r\n" + c)
	fmt.Fprint(txtCodigo, c)
	fmt.Fprint(txtCodigo, "\r\n")
}

//Registro Solo
func emitRO(op string, destino string, origen1 string, origen2 string, c string) {
	fmt.Print(" ")
	fmt.Fprint(txtCodigo, " ")
	fmt.Print(op)
	fmt.Fprint(txtCodigo, op)
	fmt.Print(" ")
	fmt.Fprint(txtCodigo, " ")
	fmt.Print(destino)
	fmt.Fprint(txtCodigo, destino)
	if origen2 != "" {
		fmt.Print(",")
		fmt.Fprint(txtCodigo, ",")
	}
	fmt.Print(origen1)
	fmt.Fprint(txtCodigo, origen1)
	fmt.Print(",")
	fmt.Fprint(txtCodigo, ",")
	fmt.Print(origen2)
	fmt.Fprint(txtCodigo, origen2)
	fmt.Print("\t" + c)
	fmt.Fprint(txtCodigo, ("\t" + c))
	fmt.Print("\r\n")
	fmt.Fprint(txtCodigo, "\r\n")
}

//Registro a Memoria
func emitRM(op string, destino string, origen string, c string) {
	fmt.Print(" ")
	fmt.Fprint(txtCodigo, " ")
	fmt.Print(op)
	fmt.Fprint(txtCodigo, op)
	fmt.Print(" ")
	fmt.Fprint(txtCodigo, " ")
	fmt.Print(destino)
	fmt.Fprint(txtCodigo, destino)
	if origen != "" {
		fmt.Print(",")
		fmt.Fprint(txtCodigo, ",")
	}
	fmt.Print(origen)
	fmt.Fprint(txtCodigo, origen)
	fmt.Print("\t" + c)
	fmt.Fprint(txtCodigo, ("\t" + c))
	fmt.Print("\r\n")
	fmt.Fprint(txtCodigo, "\r\n")
}

var msj string
var cuentaIf, cuentaWhile, cuentaDo int
var minIf, maxIf, tmpIf, minWhile, maxWhile, minDo, maxDo int
var isDo int

func genSTMT(arbol *global.Nodo) {
	var izq, der, nodoElse *global.Nodo

	switch arbol.Token.Tipo {
	case sintactico.TKN_IF:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			nodoElse = arbol.Hijos[2]
			msj = ("if_" + strconv.Itoa(cuentaIf) + ": \n")
			cuentaIf++
			maxIf++
			emitComment(msj)
			if nodoElse != nil {
				msj = ("else_" + strconv.Itoa(cuentaIf))
			} else {
				msj = ("endif_" + strconv.Itoa(cuentaIf))
			}
			cGen(izq)
			comparaSaltos(izq, "if")
			cGen(der)
			if nodoElse != nil {
				emitRM("JMP", "endif"+strconv.Itoa(cuentaIf), " ", ";salta")
				msj = ("else_" + strconv.Itoa(cuentaIf) + ": \n")
				emitComment(msj)
				hl = 0
				cGen(nodoElse.Hijos[0])
			}
			msj = ("endif_" + strconv.Itoa(cuentaIf) + ": \n")
			cuentaIf--
			if cuentaIf == minIf {
				cuentaIf = maxIf
				minIf = maxIf
			}
			emitComment(msj)

			break
		}
	case sintactico.TKN_WHILE:
		{
			isDo = 0
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			msj = ("while_" + strconv.Itoa(cuentaWhile) + ":")
			cuentaWhile++
			maxWhile++
			emitComment(msj)
			msj = ("endwhile_" + strconv.Itoa(cuentaWhile-1))
			cGen(izq)
			comparaSaltos(izq, "while")
			cGen(der)
			cuentaWhile--
			emitRM("JMP", "while"+strconv.Itoa(cuentaWhile), " ", ";salta")

			msj = ("endwhile_" + strconv.Itoa(cuentaWhile) + ":")
			emitComment(msj)

			if cuentaWhile == minWhile {
				cuentaWhile = maxWhile
				minWhile = maxWhile
			}
			break
		}
	case sintactico.TKN_UNTIL:
		{
			isDo = 1
			izq = arbol.Hijos[0]
			cuentaDo--
			msj = ("do_" + strconv.Itoa(cuentaDo))
			cGen(izq)
			comparaSaltos(izq, "do")
			break
		}
	case sintactico.TKN_DO:
		{
			isDo = 1
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			msj = ("do_" + strconv.Itoa(cuentaDo) + ": \n")
			cuentaDo++
			maxDo++
			emitComment(msj)
			cGen(izq)
			cGen(der)
			break
		}
	case sintactico.TKN_ASIGNA:
		{
			cGen(arbol.Hijos[1])
			hl = 0
			emitRM("MOV", arbol.Hijos[0].Token.Lexema, "AH", ";asignacion")
			break
		}
	case sintactico.TKN_READ:
		{
			emitRM("IN", arbol.Hijos[0].Token.Lexema, "", "\t;read")
			break
		}
	case sintactico.TKN_WRITE:
		{
			hl = 0
			cGen(arbol.Hijos[0])
			emitRM("OUT", "AH", "", "\t;write")
			break
		}
	case sintactico.TKN_ID:
		{
			if arbol.Tipo.Stmt == sintactico.T_ASIGNA {
				cGen(arbol.Hijos[1])
				hl = 0
				emitRM("MOV", arbol.Hijos[0].Token.Lexema, "AH", ";asignacion")
			}
		}
	}
}

func genEXP(arbol *global.Nodo) {
	var izq, der *global.Nodo
	switch arbol.Token.Tipo {
	case sintactico.TKN_NUMERO:
		{
			if hl == 0 {
				emitRM("MOV", "AH", arbol.Token.Lexema, ";load INT")
				hl = 1
			} else {
				emitRM("MOV", "AL", arbol.Token.Lexema, ";load INT")
				hl = 0
			}
			break
		}
	case sintactico.TKN_TRUE:
		{
			if hl == 0 {
				emitRM("MOV", "AH", arbol.Token.Lexema, ";load TRUE")
				hl = 1
			} else {
				emitRM("MOV", "AL", arbol.Token.Lexema, ";load TRUE")
				hl = 0
			}
			break
		}
	case sintactico.TKN_FALSE:
		{
			if hl == 0 {
				emitRM("MOV", "AH", arbol.Token.Lexema, ";load FALSE")
				hl = 1
			} else {
				emitRM("MOV", "AL", arbol.Token.Lexema, ";load FALSE")
				hl = 0
			}
			break
		}
	case sintactico.TKN_DECIMAL:
		{
			if hl == 0 {
				emitRM("MOV", "AH", arbol.Token.Lexema, ";load FLOAT")
				hl = 1
			} else {
				emitRM("MOV", "AL", arbol.Token.Lexema, ";load FLOAT")
				hl = 0
			}
			break
		}
	case sintactico.TKN_IGUAL, sintactico.TKN_DIFERENTE, sintactico.TKN_MAYOR, sintactico.TKN_MAYORIGUAL, sintactico.TKN_MENOR, sintactico.TKN_MENORIGUAL:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			cGen(der)
			emitRM("CMP", "AH", "AL", ";comparar")
			break
		}
	case sintactico.TKN_ID:
		{
			if hl == 0 {
				emitRM("MOV", "AH", arbol.Token.Lexema, ";load ID")
				hl = 1
			} else {
				emitRM("MOV", "AL", arbol.Token.Lexema, ";load ID")
				hl = 0
			}
			break
		}
	case sintactico.TKN_AND:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("AND", "BH", "BL", " ;op and")
			break
		}
	case sintactico.TKN_OR:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("OR", "AH", "AL", " ;op or")
			break
		}
	case sintactico.TKN_NOT:
		{
			izq = arbol.Hijos[0]
			cGen(izq)
			emitRM("NOT", "AH", "", " ;op not")
			break
		}
	case sintactico.TKN_MAS:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("ADD", "AH", "AL", " ;op +")
			break
		}
	case sintactico.TKN_MENOS:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("SUB", "AH", "AL", " ;op -")
			break
		}
	case sintactico.TKN_POR:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("MUL", "AH", "AL", " ;op *")
			break
		}
	case sintactico.TKN_ENTRE:
		{
			izq = arbol.Hijos[0]
			der = arbol.Hijos[1]
			cGen(izq)
			if izq.Token.Tipo == sintactico.TKN_NOT || izq.Token.Tipo == sintactico.TKN_OR || izq.Token.Tipo == sintactico.TKN_AND || izq.Token.Tipo == sintactico.TKN_MAS || izq.Token.Tipo == sintactico.TKN_MENOS || izq.Token.Tipo == sintactico.TKN_POR || izq.Token.Tipo == sintactico.TKN_ENTRE {
				hl = 1
			}
			cGen(der)
			emitRM("DIV", "AH", "AL", " ;op /")
			break
		}
	}
}

func comparaSaltos(nodo *global.Nodo, tipo string) {
	if tipo == "if" || tipo == "while" {
		switch nodo.Token.Tipo {
		case sintactico.TKN_IGUAL:
			{
				emitRM("JNE", msj, "", ";salta si es !=")
				break
			}
		case sintactico.TKN_DIFERENTE:
			{
				emitRM("JE", msj, "", ";salta si es ==")
				break
			}
		case sintactico.TKN_MAYOR:
			{
				emitRM("JNG", msj, "", ";salta si es <=")
				break
			}
		case sintactico.TKN_MAYORIGUAL:
			{
				emitRM("JNGE", msj, "", ";salta si es <")
				break
			}
		case sintactico.TKN_MENOR:
			{
				emitRM("JGE", msj, "", ";salta si es >=")
				break
			}
		case sintactico.TKN_MENORIGUAL:
			{
				emitRM("JG", msj, "", ";salta si es >")
				break
			}

		}
	}
	if tipo == "do" {
		switch nodo.Token.Tipo {
		case sintactico.TKN_IGUAL:
			{
				emitRM("JE", msj, "", ";salta si es ==")
				break
			}
		case sintactico.TKN_DIFERENTE:
			{
				emitRM("JNE", msj, "", ";salta si es !=")
				break
			}
		case sintactico.TKN_MAYOR:
			{
				emitRM("JG", msj, "", ";salta si es >")
				break
			}
		case sintactico.TKN_MAYORIGUAL:
			{
				emitRM("JGE", msj, "", ";salta si es >=")
				break
			}
		case sintactico.TKN_MENOR:
			{
				emitRM("JNGE", msj, "", ";salta si es <")
				break
			}
		case sintactico.TKN_MENORIGUAL:
			{
				emitRM("JNG", msj, "", ";salta si es <=")
				break
			}

		}
	}
}

func cGen(arbol *global.Nodo) {
	if arbol != nil {
		//Para solo evaluar expresiones
		if arbol.Token.Tipo == sintactico.TKN_PROGRAM && arbol.Hijos[1] != nil {
			arbol = arbol.Hijos[1]
		}
		if arbol.TipoNodo == sintactico.T_STMT {
			genSTMT(arbol)
		} else if arbol.TipoNodo == sintactico.T_EXP {
			genEXP(arbol)
		}
		cGen(arbol.Hermano)
	}
}
